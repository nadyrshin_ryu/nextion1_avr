//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _SPIM_H
#define _SPIM_H

#include "../types.h"


#define UartRxBuffSize          16

#define UART_TIMEOUT_BYTE       50      // ������� � 10 ��� ���������� �������� �������� �����


// ������������ ���� ������
#define UART_ERR_OK             1       // ��� ������ - ��������� ��� ������
#define UART_ERR_BUFF_OVF       (-1)    // ��� ������ - ������������ ������
#define UART_ERR_HW_TIMEOUT     (-2)    // ��� ������ - ���� ����-��� �������������� ��������


typedef struct
{
  //uint8_t TxRun         :1;     // ��� ��������
  //uint8_t RxRun         :1;     // ��� ����
  //uint8_t RxEnd         :1;     // ���� �������
  //uint8_t TxOvr         :1;     // 
  uint8_t RxOvr         :1;     // 
}
tUARTstate;


// ��� ��������� �� ������� ��������� ��������� ������
typedef void (*uart_RxFunc)(uint8_t *pBuff, uint16_t Len);


// ������������� UART. RxFunc - �������, ���������� ��� ��������� ������ � ��������� Nextion
void UART_Init(uint32_t BaudRate, uart_RxFunc RxFunc);
// ������� �������� ������
int8_t UART_Send(uint8_t *pBuff, uint16_t Len);


#endif